### How do I set the origin point of an object?

Select an object in `Object` mode and press `Ctrl+Alt+Shift + C` then select `Origin to 3D Cursor` to update or change the object origin to the location of the 3D cursor.

[http://blender.stackexchange.com/questions/1291/change-pivot-or-local-origin-of-an-object](http://blender.stackexchange.com/questions/1291/change-pivot-or-local-origin-of-an-object)

### How do I merge two vertices together?

1. Make sure you're in `Vertex-Select` mode`.
2. Select the first vertex.
3. Select the second vertex.
4. Hit `Alt+M` and select `first` or `last`.

[https://www.youtube.com/watch?v=taltUktxgPU](https://www.youtube.com/watch?v=taltUktxgPU)

### How do I create a face?

1. Select the vertices you want to make up your face.
2. Hit `F`.

### How do I join two objects together?

Select the two objects and click `Join` or hit `CTRL+J`.

### How do I separate two joined objects?

Hit tab, select only the faces of the object you want to separate, then hit `P`.

[https://www.youtube.com/watch?v=HnuOkJfT8Wg](https://www.youtube.com/watch?v=HnuOkJfT8Wg)

### How do I subtract one object from another?

This is called subtracting geometry and is achieved by using a `Boolean modifier`. (Blender manual link is broken.)

```
For example, adding a boolean modifier to a sphere with the operation set to Difference, the parts of the sphere which intersect the object specified in the modifier will be subtracted. As a bonus, a quick way to select an object for a modifier (or anywhere a similar selection interface is used) is by pressing E while hovering over the object selection drop down and clicking on the desired object in the 3D view.
```

1. Create an object.
2. Create another object.
3. Move the second object so it overlaps the first object.
4. Click the first object and click "Modifiers".
5. Click "Add Modifier".
6. Select "Boolean", "Difference" operation, select the second object from the list, click "Apply".

Pro Tip: Selecting the second object (the one you just subtracted from) and pressing tab will allow you to manipulate the vertices independently.

[http://blender.stackexchange.com/questions/12311/subtracting-geometry](http://blender.stackexchange.com/questions/12311/subtracting-geometry)