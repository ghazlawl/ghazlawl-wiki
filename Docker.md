### Deploy Process

1. Copy `db_env.env` file to your project folder.
2. Copy `docker_compose.yml` to your project folder.
3. Copy `docker-vh` folder to your project folder.

### Create Image

```
docker build -t tag_name ./
```

### Create Container(s)

For example, this command will create a container called `web1` and will be available at `localhost:8080`.

```
docker run -d --name web1 -p 8080:80 nginx
```

Run these commands to create or remove containers based on an image.

- View Running Containers: `docker ps`
- View All Containers: `docker ps -a`
- Start Container: `docker start -d web1`
- Stop Running Container: `docker stop web1`
- Remove Container: `docker rm {container}` (Will not remove image.)

### Start Container(s)

Run these commands to start or stop a container:

- Start New: `docker-compose up -d` (Will download images if needed and create containers.)
- Stop: `docker-compose stop`
- Start Existing: `docker-compose start`
- Stop & Remove: `docker-compose down`

### List Containers

```
docker ps -a
```

### Stop All Containers

```
docker stop `docker ps -q`
```

### SSH into Container

`docker exec -ti {container} bash`