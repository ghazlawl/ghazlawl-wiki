# Drupal 7

## Features

### My feature won't export because of a permissions issue!

To export your feature make sure that your feature folder is recursively owned by `[username]:apache` and permissions are set to `775`.

To set the owner, run this command:

```
chown -R [username]:apache [directory]
```

To set the permissions, run this command:

```
chmod -R 775 [directory]
```

Alternatively, you can run both commands at once:

```
chown -R [username]:apache [directory] && chmod -R 775 [directory]
```