# Drupal 8

## Features

### Gotchas

* Multilingual configuration is included in exported features.
* If exporting a multilingual feature make sure you visit `/admin/config/regional/content-language` and re-save to fix errors related to missing database fields.