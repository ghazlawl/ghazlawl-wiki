# SCSS/CSS

All of our custom CSS for projects is written in SCSS and compiled into CSS using Ruby and Compass.

## Installing Ruby & Compass

Compass is used to compile SCSS from the command line (or using a GUI-based program like Scout, 
Prepros, etc). Compass also includes a library of mixins that can be used to write SCSS more 
efficiently. Compass is a gem and requires Ruby to run.

### Installing Ruby

[https://www.ruby-lang.org/en/documentation/installation/](https://www.ruby-lang.org/en/documentation/installation/)

### Installing Compass

[https://teamtreehouse.com/library/compass-basics/getting-started-with-compass/installing-compass](https://teamtreehouse.com/library/compass-basics/getting-started-with-compass/installing-compass)

## Compiling SCSS

To compile SCSS you can use either a GUI-based application or the command line.

### GUI-Based Applications

When using a GUI-based application you can easily set up projects, specify the input directory, the output directory, etc.

- Prepros - https://prepros.io/
- Koala - http://koala-app.com/
- Scout - http://scout-app.io/

### Command Line

When using the command line you will need to create and configure a `config.rb` file in your project directory.

```
css_dir = "{path/to/}/css"
sass_dir = "{path/to/}/scss"
# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed
# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
```

Once the `config.rb` file is created and configured properly, you can run the following commands to 
compile your SCSS into CSS. These commands need to be run from the same directory as the `config.rb` 
file.

### How to "clean" (delete) compiled CSS files:

```
compass clean
```

### How to compile SCSS files into CSS files:

```
compass compile
```

### How to "watch" SCSS files and compile each time a file is changed:

```
compass watch
```