# ARK Dev Kit

## Cooking an Island Extension

1. All of your testing should be done in the `TestMap` map (located at `/Game/Maps/TestMap`). This map should have `PrimalGameDataOverride` set to the `PrimalGameData` in your mod directory.
2. Create a new sublevel in `/Game/Maps/TheIslandSubMaps` (the level with just a floor, light source, spawn point, etc). **DO NOT OVERRIDE THE PGD HERE. IT WILL BREAK OTHER MODS.**
3. Put any level-specific logic (dino drops, timers, etc) in the `Level Blueprint Graph`.
4. When cooking, be sure to click `Cook Island Extension`. Also, be sure to specify your levels in this order: `ARKCraftSubLevel, ARKCraftLevel` (sublevel then mod level).

## Structures

Make sure to do the following steps when copying an existing structure:

1. Copy the following files:
    1. ARKCraftCraftingTable (Structure Blueprint)
    2. EngramEntry_ARKCraftCraftingTable (Engram Blueprint)
    3. PrimalInventoryBP_ARKCraftCraftingTable (Inventory Blueprint)
    4. PrimalItemStructure_ARKCraftCraftingTable (Structure Blueprint)
2. In `PrimalGameData_BP_ARKCraft`...
    1. Add the `EngramEntry` blueprint to the `Additional Engram Blueprint Classes` list.
2. In ARKCraftCraftingTable...
    1. Set `Static Mesh` to the new mesh.
    2. Set `Pickup Gives Item` to the `PrimalItemStructure` blueprint.
    3. Set `Consumes Primal Item` to the `PrimalItemStructure` blueprint.
    4. Click `Components`, remove the existing inventory component, and add the `PrimalInventoryBP` blueprint.
3. In EngramEntry_ARKCraftCraftingTable...
    1. Set `Blue Print Entry` to the `PrimalItemStructure` blueprint.
4. In `PrimalItemStructure_ARKCraftHeadCraftingStation`...
    1. Set `Structure to Build` to the structure blueprint.